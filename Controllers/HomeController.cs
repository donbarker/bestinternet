﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;


namespace BestInternet_umbraco.Controllers
{
    public class HomeController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Home/";
        public ActionResult RenderHeader()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Header.cshtml");
        }

        public ActionResult RenderFnos()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Fnos.cshtml");
        }

        public ActionResult RenderDeals()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Deals.cshtml");
        }

        public ActionResult RenderCta()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Cta.cshtml");
        }

        public ActionResult RenderCompare()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Compare.cshtml");
        }

        public ActionResult RenderCoverageMap()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_CoverageMap.cshtml");
        }

        public ActionResult RenderProviders()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Providers.cshtml");
        }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	// Mixin content Type 1249 with alias "navigation"
	/// <summary>Navigation</summary>
	public partial interface INavigation : IPublishedContent
	{
		/// <summary>Hide Children From Navigation</summary>
		bool HideChildrenFromNavigation { get; }

		/// <summary>Hide From Navigation</summary>
		bool HideFromNavigation { get; }

		/// <summary>Hide From Search</summary>
		bool HideFromSearch { get; }

		/// <summary>Mobile Only</summary>
		bool MobileOnly { get; }

		/// <summary>Name In Navigation</summary>
		string NameInNavigation { get; }

		/// <summary>Redirect Page</summary>
		IPublishedContent UmbracoInternalRedirectId { get; }

		/// <summary>Hide</summary>
		bool UmbracoNaviHide { get; }

		/// <summary>umbracoUrlAlias</summary>
		string UmbracoUrlAlias { get; }

		/// <summary>umbracoUrlName</summary>
		string UmbracoUrlName { get; }
	}

	/// <summary>Navigation</summary>
	[PublishedContentModel("navigation")]
	public partial class Navigation : PublishedContentModel, INavigation
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "navigation";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public Navigation(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<Navigation, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Hide Children From Navigation
		///</summary>
		[ImplementPropertyType("hideChildrenFromNavigation")]
		public bool HideChildrenFromNavigation
		{
			get { return GetHideChildrenFromNavigation(this); }
		}

		/// <summary>Static getter for Hide Children From Navigation</summary>
		public static bool GetHideChildrenFromNavigation(INavigation that) { return that.GetPropertyValue<bool>("hideChildrenFromNavigation"); }

		///<summary>
		/// Hide From Navigation: Hide the node and it's children from navigation.
		///</summary>
		[ImplementPropertyType("hideFromNavigation")]
		public bool HideFromNavigation
		{
			get { return GetHideFromNavigation(this); }
		}

		/// <summary>Static getter for Hide From Navigation</summary>
		public static bool GetHideFromNavigation(INavigation that) { return that.GetPropertyValue<bool>("hideFromNavigation"); }

		///<summary>
		/// Hide From Search
		///</summary>
		[ImplementPropertyType("hideFromSearch")]
		public bool HideFromSearch
		{
			get { return GetHideFromSearch(this); }
		}

		/// <summary>Static getter for Hide From Search</summary>
		public static bool GetHideFromSearch(INavigation that) { return that.GetPropertyValue<bool>("hideFromSearch"); }

		///<summary>
		/// Mobile Only
		///</summary>
		[ImplementPropertyType("mobileOnly")]
		public bool MobileOnly
		{
			get { return GetMobileOnly(this); }
		}

		/// <summary>Static getter for Mobile Only</summary>
		public static bool GetMobileOnly(INavigation that) { return that.GetPropertyValue<bool>("mobileOnly"); }

		///<summary>
		/// Name In Navigation: This is only if you want it different to the default or if you want to use special characters in the page name
		///</summary>
		[ImplementPropertyType("nameInNavigation")]
		public string NameInNavigation
		{
			get { return GetNameInNavigation(this); }
		}

		/// <summary>Static getter for Name In Navigation</summary>
		public static string GetNameInNavigation(INavigation that) { return that.GetPropertyValue<string>("nameInNavigation"); }

		///<summary>
		/// Redirect Page
		///</summary>
		[ImplementPropertyType("umbracoInternalRedirectId")]
		public IPublishedContent UmbracoInternalRedirectId
		{
			get { return GetUmbracoInternalRedirectId(this); }
		}

		/// <summary>Static getter for Redirect Page</summary>
		public static IPublishedContent GetUmbracoInternalRedirectId(INavigation that) { return that.GetPropertyValue<IPublishedContent>("umbracoInternalRedirectId"); }

		///<summary>
		/// Hide
		///</summary>
		[ImplementPropertyType("umbracoNaviHide")]
		public bool UmbracoNaviHide
		{
			get { return GetUmbracoNaviHide(this); }
		}

		/// <summary>Static getter for Hide</summary>
		public static bool GetUmbracoNaviHide(INavigation that) { return that.GetPropertyValue<bool>("umbracoNaviHide"); }

		///<summary>
		/// umbracoUrlAlias
		///</summary>
		[ImplementPropertyType("umbracoUrlAlias")]
		public string UmbracoUrlAlias
		{
			get { return GetUmbracoUrlAlias(this); }
		}

		/// <summary>Static getter for umbracoUrlAlias</summary>
		public static string GetUmbracoUrlAlias(INavigation that) { return that.GetPropertyValue<string>("umbracoUrlAlias"); }

		///<summary>
		/// umbracoUrlName
		///</summary>
		[ImplementPropertyType("umbracoUrlName")]
		public string UmbracoUrlName
		{
			get { return GetUmbracoUrlName(this); }
		}

		/// <summary>Static getter for umbracoUrlName</summary>
		public static string GetUmbracoUrlName(INavigation that) { return that.GetPropertyValue<string>("umbracoUrlName"); }
	}
}
